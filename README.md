digraph\_search.vim
===================

This plugin provides an insert mode mapping target to search for digraphs by
searching for a substring of the character's official name, per `:help
digraph-table` and `:help digraph-table-multibyte`.  This is for situations
where you might be able to remember part of the official name for a character,
but you can't remember the digraph pieces.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
