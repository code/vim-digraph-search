"
" digraph_search.vim: Insert mode mappings to find a digraph by searching for
" its name in the contents of :help digraph-table.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_digraph_search') || &compatible || v:version < 700
  finish
endif
let loaded_digraph_search = 1

" Set up mapping
inoremap <silent> <Plug>(DigraphSearch)
      \ <C-O>:call digraph_search#()<CR>
