" Search for digraphs
function! digraph_search#() abort

  " Get the search string
  let search = input('Digraph search: ')
  if !strlen(search)
    return
  endif

  " Look for the uppercased search in the table
  let results = []
  for digraph in s:Digraphs()
    if stridx(digraph['name'], toupper(search)) != -1
      call add(results, digraph)
    endif
  endfor

  " Print results, or if there weren't any, say so
  redraw
  echo 'Digraphs matching '.toupper(search).':'
  if len(results)
    for result in results
      echo result['char']
            \.'  '.result['keys']
            \.'  '.result['name']
    endfor
  else
    echo 'None!'
  endif

endfunction

" Get private memoized list of digraph dictionary objects
function! s:Digraphs() abort

  " We haven't been called yet; get the digraph list
  if !exists('s:digraphs')
    let s:digraphs = []
    let table = 0
    let file = globpath(&runtimepath, 'doc/digraph.txt')
    if !strlen(file)
      echoerr 'Help file missing'
    endif
    for line in readfile(file)

      " Flag whether we're in one of the digraph tables; look for the heading
      let table = table && strlen(line)
            \ || line =~# '\*digraph-table\%(-mbyte\)\=\*$'
      " Skip to next line if not in a table
      if !table
        continue
      endif

      " Check whether this row matches a parseable digraph row
      let match = matchlist(line,
             \   '^\(\S\)\+\s\+'
             \ . '\(\S\S\)\s\+'
             \ . '\%(0x\)\=\x\+\s\+'
             \ . '\d\+\s\+'
             \ . '\(.\+\)'
             \ )
      " Skip to next line if not a table row match
      if !len(match)
        continue
      endif

      " Add to the digraphs list; key is digraph, value is full name
      call add(s:digraphs, {
            \ 'char': match[1],
            \ 'keys': match[2],
            \ 'name': match[3],
            \ })
    endfor
  endif

  " Return the list, whether newly-generated or from the first call
  return s:digraphs

endfunction
